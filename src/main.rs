#![allow(unused)]
extern crate sdl2;

use sdl2::pixels::Color;/*{{{*/
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::mouse::MouseButton;
use sdl2::rect::{ Rect, Point };
use sdl2::render::TextureQuery;
use sdl2::ttf;
use rand::prelude::*;

use std::path::Path;
use std::thread;
use std::time::{Duration, Instant};/*}}}*/

const INFO_HEIGHT:       u32 = 32;/*{{{*/
const CELL_SIZE:         u32 = 20;
const NUM_COLORS: [Color; 8] = [Color { r: 32 , g: 32 , b: 255, a: 255 },
                                Color { r: 32 , g: 255, b: 32 , a: 255 },
                                Color { r: 255, g: 32 , b: 32 , a: 255 },
                                Color { r: 255, g: 32 , b: 255, a: 255 },
                                Color { r: 32 , g: 32 , b: 32 , a: 255 },
                                Color { r: 200, g: 32 , b: 32 , a: 255 },
                                Color { r: 64 , g: 64 , b: 64 , a: 255 },
                                Color { r: 64 , g: 64 , b: 255, a: 255 }];/*}}}*/

#[derive(Debug, Copy, Clone, PartialEq)]/*{{{*/
enum CellState {
  Untouched,
  Marked,
  Shown,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Cell {
  cell_state: CellState,
  number: u32,
  is_clicked: bool,
  is_mine: bool,
}/*}}}*/

impl Cell {/*{{{*/
  fn empty() -> Cell {
    Cell { cell_state: CellState::Untouched, number: 0, is_clicked: false, is_mine: false }
  }
  fn new (cell_state: CellState, number: u32, is_clicked: bool, is_mine: bool) -> Cell {
    Cell { cell_state: cell_state, number: number, is_clicked: is_clicked, is_mine: is_mine }
  }
}/*}}}*/

fn main() {/*{{{*/
  //game
  let mut grid: Vec<Vec<Cell>> = vec![vec![Cell::empty(); 10]; 10];
  let mut clicked_cell: Option<(usize,usize)> = None;
  //SDL
  let sdl_context = sdl2::init().unwrap();
  let vid_subsys  = sdl_context.video().unwrap();
  let window = vid_subsys.window("Minesweeper", 800, 600)
    .position_centered().resizable().build().unwrap();
  let mut canvas = window.into_canvas().build().unwrap();
  let mut event_pump = sdl_context.event_pump().unwrap();
  let texture_creator = canvas.texture_creator();
  
  let ttf_context = ttf::init().unwrap();
  let mut font = ttf_context.load_font(Path::new("./src/LiberationMono.ttf"), INFO_HEIGHT as u16 - 12u16).unwrap();
  //etc
  let mut time = Instant::now();;
  
  canvas.set_draw_color(Color::RGB(8,8,8));
  canvas.clear();

  randomize_grid(&mut grid);
  assign_numbers(&mut grid);
  /*}}}*/
  'running: loop {/*{{{*/
    for event in event_pump.poll_iter() {
      match event {
        Event::Quit {..}                                      |
        Event::KeyDown { keycode: Some(Keycode::Escape), .. } |
        Event::KeyDown { keycode: Some(Keycode::Q), .. }        => break 'running,
        Event::MouseButtonDown { x, y, .. } => {
          let (x,y) = ((x/CELL_SIZE as i32) as usize, ((y-INFO_HEIGHT as i32)/CELL_SIZE as i32) as usize);
          let cell = grid[y][x];
          let cell_state = cell.cell_state;
          if cell_state != CellState::Shown {
            clicked_cell = Some((x,y));
            grid[y][x] = Cell { is_clicked: true, ..cell };
          }
        }
        Event::MouseButtonUp { mouse_btn, .. } => {
          if let Some((x,y)) = clicked_cell {
            let (x,y) = (x,y);
            let cell = grid[y][x];
            let cell_state = cell.cell_state;
            if cell_state != CellState::Shown {
              if mouse_btn == MouseButton::Left {
                grid[y][x] = Cell { is_clicked: false, cell_state: CellState::Shown, ..cell };
                for (x, y) in get_clear_area(&grid, (x as u32, y as u32)) {
                  let (x, y) = (x as usize, y as usize);
                  let cell = grid[y][x];
                  grid[y][x] = Cell { cell_state: CellState::Shown, ..cell };
                }
              } else if mouse_btn == MouseButton::Right {
                grid[y][x] = Cell { is_clicked: false, cell_state: CellState::Marked, ..cell };
              }
            }
          }
        }
        _ => (),
      }
    }
    
    let elapsed_secs = Instant::now().duration_since(time).as_secs();
    let timer_string = format_secs(elapsed_secs);
    
   
    draw_main(&mut canvas, &texture_creator, &ttf_context, &mut font, timer_string);
    draw_grid(&mut canvas, &texture_creator, &ttf_context, &mut font, &grid);
    canvas.present();
    thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
  }
}/*}}}*/

// SDL

fn draw_main(canvas: &mut sdl2::render::Canvas<sdl2::video::Window>, /*{{{*/
             texture_creator: &sdl2::render::TextureCreator<sdl2::video::WindowContext>,
             ttf_context: &sdl2::ttf::Sdl2TtfContext,
             font: &mut sdl2::ttf::Font<'_, '_>,
             timer: String) {
  //  prep
  //sdl
  let window = canvas.window_mut();
  let (x,y)  = window.size();
  //time
  canvas.set_draw_color(Color::RGB(4,4,4));
  canvas.clear();
  //  info bar
  canvas.set_draw_color(Color::RGB(64,64,64));
  canvas.fill_rect(Rect::new(0,0, x,INFO_HEIGHT));
  //  text
  let surface = font.render(timer.as_str()).blended(Color::RGB(255,255,255)).unwrap();
  let texture = texture_creator.create_texture_from_surface(&surface).unwrap();
  let TextureQuery { width, height, .. } = texture.query();
  let text_point = Point::new((x as f32*0.5) as i32, (INFO_HEIGHT/2) as i32);
  //background
  canvas.set_draw_color(Color::RGB(4,4,4));
  canvas.fill_rect(Rect::from_center(text_point, width+4, INFO_HEIGHT-4));
  //main
  canvas.copy(&texture, None, Rect::from_center(text_point, width, height));
}/*}}}*/

fn draw_grid(mut canvas: &mut sdl2::render::Canvas<sdl2::video::Window>, /*{{{*/
             texture_creator: &sdl2::render::TextureCreator<sdl2::video::WindowContext>,
             ttf_context: &sdl2::ttf::Sdl2TtfContext,
             mut font: &mut sdl2::ttf::Font<'_, '_>,
             grid: &Vec<Vec<Cell>>) {
 for y in 0..grid.len() {
    for x in 0..grid[y].len() {
      let cell = &grid[y][x];
      draw_cell(&mut canvas, &texture_creator, &ttf_context, &mut font, &cell, (x as u32, y as u32));
    }
  }
}/*}}}*/

fn draw_cell(canvas: &mut sdl2::render::Canvas<sdl2::video::Window>, /*{{{*/
             texture_creator: &sdl2::render::TextureCreator<sdl2::video::WindowContext>,
             ttf_context: &sdl2::ttf::Sdl2TtfContext,
             font: &mut sdl2::ttf::Font<'_, '_>,
             Cell { cell_state, number, is_clicked, is_mine, ..}: &Cell,
             (x,y): (u32, u32)) {
  
  let color = match cell_state {
    CellState::Untouched => if *is_clicked {
      Color::RGB(128,128,128)
    } else {
      Color::RGB(180,180,180)
    },
    CellState::Marked => if *is_clicked {
      Color::RGB(148, 138, 0)
    } else {
      Color::RGB(200, 190, 10)
    },
    CellState::Shown => if *is_mine {
      Color::RGB(255,0,0)
    } else {
      Color::RGB(80,80,80)
    },
  
};
  canvas.set_draw_color(color);
  canvas.fill_rect(Rect::new((x*CELL_SIZE+1) as i32, (y*CELL_SIZE+INFO_HEIGHT+1) as i32,
                             CELL_SIZE-2, CELL_SIZE-2));

  if *cell_state == CellState::Shown && number > &0 && !is_mine {
    let surface = font.render(number.to_string().as_str()).blended(NUM_COLORS[(number-1) as usize]).unwrap();
    let texture = texture_creator.create_texture_from_surface(&surface).unwrap();
    let TextureQuery { width, height, .. } = texture.query();
    let text_point = Point::new((x*CELL_SIZE+CELL_SIZE/2) as i32,
                                (y*CELL_SIZE+INFO_HEIGHT+CELL_SIZE/2+2) as i32);
    canvas.copy(&texture, None, Rect::from_center(text_point, width, height));
  }
}/*}}}*/

// etc

fn format_secs(total_seconds: u64) -> String { /*{{{*/
  let minutes = total_seconds / 60;
  let seconds = total_seconds - minutes*60;
  format!("{:0>2}:{:0>2}", minutes, seconds)
}/*}}}*/

//game

fn get_adjacent_cells(grid: &Vec<Vec<Cell>>, (x,y): (u32,u32)) -> Vec<(i32, i32)> { /*{{{*/
  let (xi32, yi32) = (x as i32, y as i32);
  vec![(xi32-1,yi32-1), (xi32,yi32-1), (xi32+1,yi32-1),
       (xi32-1,yi32),                  (xi32+1,yi32),
       (xi32-1,yi32+1), (xi32,yi32+1), (xi32+1,yi32+1)]
    .iter()
    .filter(|(x,y)| !(x < &0 || x > &9 || y < &0 || y > &9))
    .map(|i| *i)
    .collect::<Vec<(i32,i32)>>()
  
}/*}}}*/

fn get_adjacent_mines(grid: &Vec<Vec<Cell>>, (x,y): (u32,u32)) -> Vec<(i32,i32)> {/*{{{*/
  let (xi32, yi32) = (x as i32, y as i32);
  get_adjacent_cells(grid, (x,y))
    .iter()
    .filter(|(x,y)| { let Cell { is_mine, .. } = grid[*y as usize][*x as usize]; is_mine })
    .map(|i| *i)
    .collect::<Vec<(i32,i32)>>()
}/*}}}*/

fn get_clear_area(grid: &Vec<Vec<Cell>>, (x,y): (u32, u32)) -> Vec<(i32,i32)> {/*{{{*/
  let Cell { number, is_mine, .. } = grid[y as usize][x as usize];
  if number != 0 || is_mine {
    return Vec::new();
  }

  let mut coords: Vec<(i32,i32)> = get_adjacent_cells(&grid, (x,y));
  loop {
    coords.sort();
    coords.dedup();
    let mut thing: Vec<(i32,i32)> = coords.clone();
    thing = thing.iter()
      .map(|(x,y)| (*x as u32, *y as u32))
      .flat_map(|c| get_adjacent_cells(&grid, c))
      .filter(|(x,y)| if let Cell { number: 0, is_mine: false, .. } = grid[*y as usize][*x as usize] {true}else{false})
      .collect::<Vec<(i32,i32)>>();
    thing.sort();
    thing.dedup();
    
    if thing != coords {
      coords = thing;
    } else {
      thing = thing.iter()
        .map(|(x,y)| (*x as u32, *y as u32))
        .flat_map(|c| get_adjacent_cells(&grid, c))
        .collect::<Vec<(i32, i32)>>();
      thing.sort();
      thing.dedup();
      coords = thing;
      break;
    }
  }
  
  coords
}/*}}}*/

fn randomize_grid(grid: &mut Vec<Vec<Cell>>) {/*{{{*/
  let mut rng = thread_rng();
  let mut done = false; // TODO - wut
  let mut coords: Vec<(u32,u32)> = vec![];

  while !done {
    let coord = (rng.gen_range(0,10), rng.gen_range(0,10)); // TODO: substitute hardcoded size?
    if coords.contains(&coord) {
      continue;
    } else {
      coords.push(coord);
    }
    if coords.len() == 10 {
      break;
    }
  }

  for (x_,y_) in coords {
    let (x,y) = (x_ as usize, y_ as usize);
    let cell = grid[y][x];
    grid[y][x] = Cell { is_mine: true, ..cell };
  }
}/*}}}*/

fn assign_numbers(grid: &mut Vec<Vec<Cell>>) {/*{{{*/
  for x in 0..10 {
    for y in 0..10 {
      let num: u32 = get_adjacent_mines(&grid, (x as u32, y as u32)).iter().count() as u32;
      let cell = grid[y][x];
      grid[y][x] = Cell { number:  num, ..cell };
    }
  }
}/*}}}*/

